#!/sbin/sh
para_list=$@

#operate date begin
start=$(date "+%Y-%m-%d %H:%M:%S")
echo "operate data begin: ${start}"

#You should add your sh operation in your own area you belongs to
#=====================OS Begin============================
#ifdef COLOROS_EDIT
#shaochou.qiu@ROM.SysApp.HRO, 2019/06/04, Add for bug id 2048926|2032580
#ota from O to P uid changed

mkdir -p /data/media/0/.coloros/systemui
if [ -d "/data/user_de/0/com.android.systemui/files/edge_panel" ]; then
  cp -rf /data/user_de/0/com.android.systemui/files/edge_panel/ /data/media/0/.coloros/systemui/
fi
mkdir -p /data/media/0/.coloros/systemui/edge_panel/shared_prefs
if [ -f "/data/user_de/0/com.android.systemui/shared_prefs/edge_panel_prefs.xml" ]; then
  cp -rf /data/user_de/0/com.android.systemui/shared_prefs/edge_panel_prefs.xml /data/media/0/.coloros/systemui/edge_panel/shared_prefs/
fi

if [ -f "/data/user_de/0/com.android.systemui/shared_prefs/envelope_pre_key.xml" ]; then
  cp -rf /data/user_de/0/com.android.systemui/shared_prefs/envelope_pre_key.xml /data/media/0/.coloros/systemui/edge_panel/shared_prefs/
fi

#operate systemui date end
end=$(date "+%Y-%m-%d %H:%M:%S")
echo "operate systemui data end: ${end}"

#W9001280, 2019/06/11, Add for bug id 2065496|2035548
#ota from O to P uid changed
mkdir -p /data/media/0/.coloros/providers/downloads
if [ -d "/data/data/com.android.providers.downloads/databases" ]; then
  cp -rf /data/data/com.android.providers.downloads/databases /data/media/0/.coloros/providers/downloads/
fi

#operate downloadprovider date end
end=$(date "+%Y-%m-%d %H:%M:%S")
echo "downloadprovider data end: ${end}"

#endif /* COLOROS_EDIT */
#=====================OS End==============================

#=====================Android Begin========================
#=====================Android End==========================

#=====================BSP Begin============================
#=====================BSP End==============================

#=====================Multi Begin==========================
#=====================Multi End============================

#=====================NetWork Begin========================
#=====================NetWork End==========================

#operate end
end=$(date "+%Y-%m-%d %H:%M:%S")
echo "operate data end: ${end}"

interval=$(($(($(date +%s -d "$end")-$(date +%s -d "$start")))))
echo "operate data cost: $interval seconds"