#!/sbin/sh

toybox start wmt_loader
toybox start wmt_launcher
insmod /sbin/wmt_drv.ko
insmod /sbin/wmt_chrdev_wifi.ko
insmod /sbin/wlan_drv_gen3.ko
insmod /sbin/wlan_drv_gen4m.ko
