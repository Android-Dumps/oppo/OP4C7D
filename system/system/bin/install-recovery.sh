#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:9befcb4ebcf60ecf7482c083778100aaf960c8e2; then
  applypatch  EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:0f9360f9fc96cdf47ba876767b7efc9949362925 EMMC:/dev/block/platform/bootdevice/by-name/recovery 9befcb4ebcf60ecf7482c083778100aaf960c8e2 33554432 0f9360f9fc96cdf47ba876767b7efc9949362925:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
